package com.example.cheque;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.cheque.ui.login.LoginActivity;

public class menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        activarBotones();
    }


    private void activarBotones() {
        Button btSalir = (Button) findViewById(R.id.btSalir);
        btSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accionBotonSalir();
            }
        });

        Button btAgregarCheque = (Button) findViewById(R.id.btAgregarCheque);
        btAgregarCheque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accionBotonAgregarCheque();
            }
        });

        Button btVerHistorial = (Button) findViewById(R.id.btVerHistorial);
        btVerHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accionBotonVerHistorial();
            }
        });
    }

    private void accionBotonSalir() { finishAffinity(); }



    private void accionBotonAgregarCheque() {
        Intent agregarcheque = new Intent(this, com.example.cheque.agregarcheque.class);
        startActivity(agregarcheque);
    }

    private void accionBotonVerHistorial() {
        Intent verhistorial = new Intent (this, com.example.cheque.verhistorial.class);
        startActivity(verhistorial);
    }
}